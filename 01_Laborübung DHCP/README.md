# Laborübung 1: DHCP Server-Konfiguration aus einem Cisco Router auslesen

Vollständige Aufgabenstellung: https://gitlab.com/alptbz/m123/-/blob/main/05_DHCP/01_DHCP%20Server%20Konfiguration%20Cisco.md

## Erledigte Vorbereitungen
 - GNS3 Projekt importiert

## 2. Router starten, einloggen und konfigurierte IP-Adressen auslesen

 - Welche IP-Adresse ist auf dem *Interface* *GigabitEthernet0/0* und *GigabitEthernet0/1* konfiguriert?
   - 192.168.8.1/24
 - Ist das *Interface* *GigabitEthernet0/2* aktiv? Welchen Status hat es?
   - UP/UP
 - Wo ist das *Interface* *GigabitEthernet0/1* in der Netzwerkgrafik zu finden?
![Interface ist da](images/dainterface.PNG)

## 3. Konfiguration anzeigen
 - Mit welchem Befehl (wie lautet die Zeile) wurde der Name des Routers konfiguriert?
   - Lorem ipsum dolor sit amet, consectetur adipiscing elit, se....
 - Wie lautet das Passwort für den *remote access* mit TELNET?
   - LOREMIPSUM

## 4. DHCP Konfiguration
...usw.

# Weiterführende Ressourcen 
 - Cisco CLI for Beginners: https://www.youtube.com/watch?v=xOqwxluUCc8 (Für GNS3 Relevante CLI Infos Ab 08:19)
 - Using Cisco IOS Command Line: https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/fundamentals/configuration/15mt/fundamentals-15-mt-book/cf-cli-basics.html 

## Neue Lerninhalte
Das war für mich neu: 
 - Cisco CLI Befehle
 - MikroTik RouterOS war für mich komplett neue (Keine Erfahrungen)
 - IPv4-Adressen auf RouterOS konfigurieren
 - Erfahrungen zur Bedienung von GNS3 habe ich bereits in der ersten Übung gesammelt. Neu war die "Edit config" funktion. 

## Reflexion
Mithilfe der Übung habe ich nun das Gefühl eine grobe Übersicht über GNS3 zu haben. Der [Kompetenz](../01_Kompetenzmatrix/README.md) *D1G: Ich kann einfache statische Routingtabellen umsetzen.* bin ich nun definitiv einen Schritt näher gekommen. 
Ich fand die Übung sehr einfach und hatte keine grosse Mühe. 
